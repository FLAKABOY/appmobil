package com.example.appholamundo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {

    private Spinner sp;

    private Button btnCerrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        btnCerrar = findViewById(R.id.btnCerrar);

        ArrayList<ItemDataSpinner> list = new ArrayList<>();
        list.add(new ItemDataSpinner(getString(R.string.itemFrappses),getString(R.string.msgFrapses), R.mipmap.categorias));
        list.add(new ItemDataSpinner(getString(R.string.itemAgradecimiento),getString(R.string.msgAgradecimiento), R.mipmap.agradecimiento));
        list.add(new ItemDataSpinner(getString(R.string.itemAmor),getString(R.string.msgAmor), R.mipmap.corazon));
        list.add(new ItemDataSpinner(getString(R.string.itemNewYear),getString(R.string.msgNewYear), R.mipmap.nuevo));
        list.add(new ItemDataSpinner(getString(R.string.itemCanciones),getString(R.string.msgCanciones), R.mipmap.canciones));

        sp = findViewById(R.id.spinner1);
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spiner_layout, R.id.lblCategorias, list);
        sp.setAdapter(adapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),getString(R.string.msgSeleccionado).toString()+ " "+ ((ItemDataSpinner) parent.getItemAtPosition(position)).getTextCategoria(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
