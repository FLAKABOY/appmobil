package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class cotizacionIngresoActivity extends AppCompatActivity {

    private EditText clientNameEditText;
    private Button quoteButton;
    private Button btnCerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion_ingreso);

        clientNameEditText = findViewById(R.id.clientNameEditText);
        quoteButton = findViewById(R.id.quoteButton);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        quoteButton.setOnClickListener(view -> {
            String clientName = clientNameEditText.getText().toString();
            if(clientName.isEmpty()){
                Toast.makeText(getApplicationContext(), "Faltan datos por llenar", Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = new Intent(cotizacionIngresoActivity.this, cotizacionActivity.class);
            intent.putExtra("CLIENT_NAME", clientName);
            startActivity(intent);
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}